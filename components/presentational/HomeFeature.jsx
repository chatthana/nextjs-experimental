export default () => (
  <div>
    <div className="row">
      <div className="col">
        <div className="card">
          <div className="card-body">
            <h3>Works @ Lightspeed</h3>
            <p>
              Nothing to explain here, it is fast like hell becasue we embrace
              all of the functionalities of Next.js to boost up the build
            </p>
            <button className="btn btn-primary">See plans</button>
          </div>
        </div>
      </div>
      <div className="col">
        <div className="card">
          <div className="card-body">
            <h3>Reliable</h3>
            <p>
              How could you impeace someone who's done a great job and everybody loves
              to have the systems that work fast
            </p>
            <button className="btn btn-primary">Learn More</button>
          </div>
        </div>
      </div>
    </div>
  </div>
);