import Link from 'next/link';

export default () => (
  <div className="header">
    <div className="navbar navbar-dark bg-dark navbar-expand-lg">
      <div className="container">
        <Link href="/"><a className="navbar-brand">Fire<span style={{fontWeight: 800}}>Man</span></a></Link>
        <div className ="collapse navbar-collapse">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item active">
              <Link href="/about"><a className="nav-link">About</a></Link>
            </li>
            <li className="nav-item">
              <Link href="/portfolio"><a className="nav-link">Portfolio</a></Link>
            </li>
            <li className="nav-item">
              <Link href="/products"><a className="nav-link">Products</a></Link>
            </li>
            <li className="nav-item">
              <Link href="/contact"><a className="nav-link" href="#">Contact us</a></Link>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
);