import React, { Component } from 'react';
import FrontEnd from '../components/layouts/frontend';
export default class extends Component {
  render() {
    return (
      <FrontEnd>
        <div className="container">
          <div className="row">
            <div className="col">
              <h3 style={{marginTop: '30px'}}>Welcome to the next generation</h3>
            </div>
          </div>
        </div>
      </FrontEnd>
    );
  }
}