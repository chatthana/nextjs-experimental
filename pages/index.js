import React, { Component } from 'react';
import Link from 'next/link';
import FrontEnd from '../components/layouts/frontend';
import HomeFeature from '../components/presentational/HomeFeature';

import '../stylesheets/style.styl';

export default class extends Component {
  render() {
    return (
      <div>
        <FrontEnd>
          <div className="container">
            <div className="row">
              <div className="col">
                <div style={{marginTop: '20px', textAlign: 'center'}}>
                  <div className="img-container extreme-padded">
                    <img src="/static/images/fire-sample.png" />
                  </div>
                  <h2>The next generation of JavaScript Framework</h2>
                  <p>
                    Looking for a great music sound production house! Here is what you are looking for.
                  </p>
                  <Link href="/about"><a className="btn btn-danger">Learn more</a></Link>
                </div>
              </div>
            </div>
            <div style={{marginTop: '30px'}}>
              <HomeFeature />
            </div>
          </div>
        </FrontEnd>
      </div>
    )
  }
}